<div class="container mt-4">
	<table class="table table-striped">
		<thead>
		<tr>
			<th scope="col">Id</th>
			<th scope="col">Nome</th>
			<th scope="col">Deletar</th>
			<th scope="col">Alterar</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($jogos as $jogo): ?>
		<tr>
			<th scope="row"><?= $jogo->id ?></th>
			<td><?= $jogo->nome?></td>
			<td><a href="<?= base_url('jogos/delete_jogo/'.$jogo->id) ?>"><i class="fas fa-trash"></i></a></td>
			<td><a href="<?= base_url('jogos/alterar_jogo/'.$jogo->id)?>"><i class="fas fa-pencil-alt"></i></a></td>
		</tr>
		<tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
