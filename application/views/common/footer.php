<footer class="navbar-fixed-bottom page-footer font-small mt-5 green pt-4 ">
	<div class="container-fluid text-center text-md-left">
		<div class="row">
			<div class="col-md-12 text-center mt-md-0 mt-3">
				<h5 class="text-uppercase">IFSP Jogos</h5>
				<p>O IFSP Jogos tem o objetivo de disponibilizar jogos para os seus alunos.</p>
			</div>
		</div>
	</div>
	<div class="footer-copyright text-center py-3">© 2019 Copyright:
		<a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
	</div>
</footer>
