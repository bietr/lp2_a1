<div class="container mt-2">
	<div class="row">
		<div class="jumbotron text-center">

			<h4 class="card-title h4"><strong>Quem Somos?</strong></h4>

			<div class="view overlay my-4">
				<img src="<?= base_url('assets/img/gamers.jpg') ?>" class="img-fluid" alt="">
				<a href="<?= base_url('') ?>">
					<div class="mask rgba-white-slight"></div>
				</a>
			</div>

			<h5 class="indigo-text h5 mb-4">IFSP Jogos</h5>

			<p class="card-text text-justify">O IFSP Jogos foi fundado em 2019 para que os alunos do IFSP Guarulhos tivessem acesso a todos os jogos que estão sendo lançados e também possam achar outros colegas que estão inseridos nessa mesma cultura gamer.</p>
		</div>
	</div>
</div>
