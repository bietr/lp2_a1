<div class="container">
   <h1 class="mt-5 gradient">Veja nosso acervo:</h1>
  <div class="row mt-5">

	  <?php foreach($jogos as $jogo):?>
	  <div class="col-md-4 mb-3">
      <img src="assets/img/<?= $jogo->image ?>.jpg" class="rounded img-fluid"
        alt="AC Odyssey">
		<p class="text-center lead monospace"><?= $jogo->nome ?></p>
    </div>
	  <?php endforeach; ?>
<!--    <div class="col-md-4 mb-3">-->
<!--      <img src="assets/img/game2.jpg" class="rounded img-fluid"-->
<!--        alt="Red Dead Redemption 2">-->
<!--		<p class="text-center lead monospace">Red Dead Redemption 2</p>-->
<!--    </div>-->
<!--    <div class="col-md-4 mb-3">-->
<!--      <img src="assets/img/game3.jpg" class="rounded img-fluid"-->
<!--        alt="Resident Evil 2 Remake">-->
<!--		<p class="text-center lead monospace">Resident Evil 2 Remake</p>-->
<!--    </div>-->
  </div>
</div>
