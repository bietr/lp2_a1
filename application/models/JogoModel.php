<?php

defined('BASEPATH') OR exit('NO direct script access allowed');

class JogoModel extends CI_Model{

	public function get_data(){
		$sql = "SELECT * FROM jogo";
		$res = $this->db->query($sql);
		$data = $res->result();
		return $data;
	}
	public function delete_jogo($id) {
		$this->db->where('id', $id);
		return $this->db->delete('jogo');
	}
	public function get_jogo($id){
		$sql = "SELECT * FROM jogo WHERE id='$id'";
		$res = $this->db->query($sql);
		$data = $res->result_array();
		return $data[0];
	}
	public function editar_jogo($id){
		if($this->input->post()) {
			$image = $this->input->post('image');
			$nome = $this->input->post('nome');
			$this->db->set('nome', $nome);
			$this->db->set('image', $image);
			$this->db->where('id', $id);
			$this->db->update('jogo');
			echo '<script>alert("Jogo editado com sucesso!")</script>';
			redirect('jogos/administrativo');
		}
	}

}
?>
